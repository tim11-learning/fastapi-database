from typing import Type
from sqlalchemy.orm import Session, Query
from sqlalchemy.exc import IntegrityError
from backend import orm
from backend.common.exceptions import CrudException, CrudNotFoundException


class Crud:
    @staticmethod
    def all(session: Session, aircraft_code: str = None) -> list[Type[orm.Seats]]:
        query = session.query(orm.Seats)
        if aircraft_code:
            query = query.filter(orm.Seats.aircraft_code.like(f'{aircraft_code}'))
        return query.all()

    @staticmethod
    def one(session: Session, aircraft_code: str, seat_no: str, only_query: bool = False) -> orm.Seats | Query | None:
        query = session.query(orm.Seats).filter(
            orm.Seats.aircraft_code == aircraft_code,
            orm.Seats.seat_no == seat_no
        )
        return query if only_query else query.first()

    @staticmethod
    def create(session: Session, data: dict) -> orm.Seats | CrudException:
        item = orm.Seats(**data)
        try:
            session.add(item)
            session.commit()
        except IntegrityError as e:
            session.rollback()
            raise CrudException(str(e.__cause__))
        session.refresh(item)
        return item

    @staticmethod
    def update(session: Session, aircraft_code: str, seat_no: str, data: dict) -> orm.Seats | CrudException | CrudNotFoundException:
        item = Crud.one(session, aircraft_code, seat_no)
        if not item:
            raise CrudNotFoundException(f'Seat in aircraft {aircraft_code} with number = {seat_no} not found')
        try:
            item.fill(data)
            session.add(item)
            session.commit()
        except IntegrityError as e:
            session.rollback()
            raise CrudException(str(e.__cause__))
        session.refresh(item)
        return item

    @staticmethod
    def delete(session: Session, aircraft_code: str, seat_no: str) -> CrudException | CrudNotFoundException | None:
        item = Crud.one(session, aircraft_code, seat_no)
        if not item:
            raise CrudNotFoundException(f'Seat in aircraft {aircraft_code} with number = {seat_no} not found')
        try:
            Crud.one(session, aircraft_code, seat_no, True).delete()
            session.commit()
        except IntegrityError as e:
            session.rollback()
            raise CrudException(str(e.__cause__))
        return None
