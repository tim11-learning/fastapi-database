from typing import Type
from sqlalchemy.orm import Session, Query
from sqlalchemy.exc import IntegrityError
from backend import orm
from backend.common.exceptions import CrudException, CrudNotFoundException


class Crud:
    @staticmethod
    def all(session: Session) -> list[Type[orm.Regions]]:
        return session.query(orm.Regions).all()

    @staticmethod
    def one(session: Session, _id: int, only_query: bool = False) -> orm.Regions | Query | None:
        query = session.query(orm.Regions).filter(orm.Regions.id == _id)
        return query if only_query else query.first()

    @staticmethod
    def create(session: Session, data: dict) -> orm.Regions | CrudException:
        item = orm.Regions(**data)
        try:
            session.add(item)
            session.commit()
        except IntegrityError as e:
            session.rollback()
            raise CrudException(str(e.__cause__))
        session.refresh(item)
        return item

    @staticmethod
    def update(session: Session, _id: int, data: dict) -> orm.Regions | CrudException | CrudNotFoundException:
        item = Crud.one(session, _id)
        if not item:
            raise CrudNotFoundException(f'Region with id = {_id} not found')
        try:
            item.fill(data)
            session.add(item)
            session.commit()
        except IntegrityError as e:
            session.rollback()
            raise CrudException(str(e.__cause__))
        session.refresh(item)
        return item

    @staticmethod
    def delete(session: Session, _id: int) -> CrudException | CrudNotFoundException | None:
        item = Crud.one(session, _id)
        if not item:
            raise CrudNotFoundException(f'Region with id = {_id} not found')
        try:
            Crud.one(session, _id, True).delete()
            session.commit()
        except IntegrityError as e:
            session.rollback()
            raise CrudException(str(e.__cause__))
        return None
