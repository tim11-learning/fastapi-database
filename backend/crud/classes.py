from typing import Type
from sqlalchemy.orm import Session, Query
from sqlalchemy.exc import IntegrityError
from backend import orm
from backend.common.exceptions import CrudException, CrudNotFoundException


class Crud:
    @staticmethod
    def all(session: Session) -> list[Type[orm.Classes]]:
        return session.query(orm.Classes).all()

    @staticmethod
    def one(session: Session, _id: int, only_query: bool = False) -> orm.Classes | Query | None:
        query = session.query(orm.Classes).filter(orm.Classes.id == _id)
        return query if only_query else query.first()

    @staticmethod
    def create(session: Session, data: dict) -> orm.Classes | CrudException:
        item = orm.Classes(**data)
        try:
            session.add(item)
            session.commit()
        except IntegrityError as e:
            session.rollback()
            raise CrudException(str(e.__cause__))
        session.refresh(item)
        return item

    @staticmethod
    def update(session: Session, _id: int, data: dict) -> orm.Classes | CrudException | CrudNotFoundException:
        item = Crud.one(session, _id)
        if not item:
            raise CrudNotFoundException(f'Class with id = {_id} not found')
        try:
            item.fill(data)
            session.add(item)
            session.commit()
        except IntegrityError as e:
            session.rollback()
            raise CrudException(str(e.__cause__))
        session.refresh(item)
        return item

    @staticmethod
    def delete(session: Session, _id: int) -> CrudException | CrudNotFoundException | None:
        item = Crud.one(session, _id)
        if not item:
            raise CrudNotFoundException(f'Class with id = {_id} not found')
        try:
            Crud.one(session, _id, True).delete()
            session.commit()
        except IntegrityError as e:
            session.rollback()
            raise CrudException(str(e.__cause__))
        return None
