from .regions import Crud as CrudRegions
from .classes import Crud as CrudClasses
from .aircrafts import Crud as CrudAircrafts
from .seats import Crud as CrudSeats
from .cities import Crud as CrudCities

__all__ = ['CrudRegions', 'CrudClasses', 'CrudAircrafts', 'CrudSeats', 'CrudCities']
