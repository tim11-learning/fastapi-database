from typing import Type
from sqlalchemy.orm import Session, Query
from sqlalchemy.exc import IntegrityError
from backend import orm
from backend.common.exceptions import CrudException, CrudNotFoundException


class Crud:
    @staticmethod
    def all(session: Session, region_id: int = None) -> list[Type[orm.Cities]]:
        query = session.query(orm.Cities)
        if region_id:
            query = query.filter(orm.Cities.region_id == region_id)
        return query.all()

    @staticmethod
    def one(session: Session, city_id: int, only_query: bool = False) -> orm.Cities | Query | None:
        query = session.query(orm.Cities).filter(orm.Cities.id == city_id)
        return query if only_query else query.first()

    @staticmethod
    def create(session: Session, data: dict) -> orm.Cities | CrudException:
        item = orm.Cities(**data)
        try:
            session.add(item)
            session.commit()
        except IntegrityError as e:
            session.rollback()
            raise CrudException(str(e.__cause__))
        session.refresh(item)
        return item

    @staticmethod
    def update(session: Session, city_id, data: dict) -> orm.Cities | CrudException | CrudNotFoundException:
        item = Crud.one(session, city_id)
        if not item:
            raise CrudNotFoundException(f'City with id = {city_id} not found')
        try:
            item.fill(data)
            session.add(item)
            session.commit()
        except IntegrityError as e:
            session.rollback()
            raise CrudException(str(e.__cause__))
        session.refresh(item)
        return item

    @staticmethod
    def delete(session: Session, city_id: int) -> CrudException | CrudNotFoundException | None:
        item = Crud.one(session, city_id)
        if not item:
            raise CrudNotFoundException(f'City with id = {city_id} not found')
        try:
            Crud.one(session, city_id, True).delete()
            session.commit()
        except IntegrityError as e:
            session.rollback()
            raise CrudException(str(e.__cause__))
        return None
