import sys
sys.path.insert(1, '../')
import asyncio
import uvicorn
from os import path
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from backend.common import Config, Database
from backend.routes import routes


class SqlLiteConfig(Config):
    pass


config = SqlLiteConfig(path.abspath('.env'))
Database.config(config)

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*']
)
for route in routes:
    app.include_router(route)


async def main():
    web_config = uvicorn.Config(
        'main:app',
        host=config.get('WEB_HOST', '127.0.0.1'),
        port=int(config.get('WEB_PORT', 5000)),
        reload=bool(config.get('DEBUG')),
        log_level=config.get('WEB_LOG_LEVEL', 'info' if not bool(config.get('DEBUG')) else 'debug')
    )
    server = uvicorn.Server(web_config)
    await server.serve()


if __name__ == '__main__':
    asyncio.run(main())
