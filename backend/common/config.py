from multipledispatch import dispatch
from typing import Any
from environs import Env
from os import path

SUPPORTED_DB_TYPES = ['sqlite', 'postgres']


class Config:
    __storage: dict | Env = {}
    __dsn: str = None

    @dispatch(dict)
    def __init__(self, data: dict):
        for key, value in data.items():
            self.__storage[str(key).upper()] = value
        self.check()

    @dispatch(str)
    def __init__(self, filename: str):
        if not path.exists(filename) or not (path.isfile(filename) or path.islink(filename)):
            raise Exception('Problem with config file')
        env = Env()
        env.read_env(filename)
        self.__storage = env
        self.check()

    def check(self) -> bool | Exception:
        db_type = self.get('DB_TYPE')
        if not db_type:
            raise Exception('DB_TYPE is undefined')
        if db_type not in SUPPORTED_DB_TYPES:
            raise Exception(f'DB_TYPE must have one value from {",".join(SUPPORTED_DB_TYPES)}')
        if db_type == 'sqlite':
            database_file = self.get('DB_FILE')
            if not database_file:
                raise Exception('DB_FILE is undefined')
            database_path = path.abspath(database_file)
            self.__dsn = f'sqlite:///{database_file}'
        elif db_type == 'postgres':
            pg_host = self.get('POSTGRES_HOST', '127.0.0.1')
            pg_port = self.get('POSTGRES_PORT', '5432')
            pg_name = self.get('POSTGRES_DB')
            pg_user = self.get('POSTGRES_USER')
            pg_pass = self.get('POSTGRES_PASSWORD')
            if not pg_name:
                raise Exception('POSTGRES_DB is undefined')
            if not pg_user:
                raise Exception('POSTGRES_USER is undefined')
            if not pg_pass:
                raise Exception('POSTGRES_PASSWORD is undefined')
            self.__dsn = f'postgresql://{pg_user}:{pg_pass}@{pg_host}:{pg_port}/{pg_name}'
        return True

    def get(self, key: str, default: Any = None) -> Any | None:

        if type(self.__storage) == Env:
            return self.__storage(key.upper(), default=default)
        return self.__storage.get(key.upper(), default)

    @property
    def dsn(self) -> str | None:
        return self.__dsn


