class CrudException(Exception):
    pass


class CrudNotFoundException(Exception):
    pass
