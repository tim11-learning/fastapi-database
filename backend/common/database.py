from sqlalchemy.orm import Session, sessionmaker, scoped_session
from sqlalchemy.engine import create_engine
import logging
from backend.common import Config
from backend.orm import Base as OrmBase

logger = logging.getLogger(__name__)

class Database:
    __session_maker = None

    @staticmethod
    def config(config: Config):
        engine = create_engine(
            config.dsn,
            echo=bool(config.get('debug', False)),
            pool_size=20,
            max_overflow=0,
            pool_pre_ping=True,
            pool_recycle=3600,
            connect_args={
                "keepalives": 1,
                "keepalives_idle": 30,
                "keepalives_interval": 10,
                "keepalives_count": 5
            }
        )
        Database.__session_maker = scoped_session(
            sessionmaker(
                bind=engine,
                autoflush=False,
                autocommit=False
            )
        )
        OrmBase.metadata.create_all(engine)

    @staticmethod
    def get_session() -> Session | Exception:
        return Database.__session_maker()
