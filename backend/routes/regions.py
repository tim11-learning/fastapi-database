from fastapi import APIRouter, Depends, HTTPException
from typing import List
from sqlalchemy.orm import Session
from backend import models
from backend.common.exceptions import CrudException, CrudNotFoundException
from backend.crud import CrudRegions
from backend.dependencies import get_session, get_token_header



router = APIRouter(
    prefix='/regions',
    tags=['regions'],
    responses={
        404: {
            'description': 'Method not found'
        }
    }
)


@router.get('', response_model=List[models.Region])
async def get_all(session: Session = Depends(get_session)):
    return CrudRegions.all(session)


@router.get('/{region_id}', response_model=models.Region)
async def get_by_id(region_id: int, session: Session = Depends(get_session)):
    item = CrudRegions.one(session, region_id)
    if not item:
        raise HTTPException(status_code=404, detail=f'Region with id = {region_id} not found')
    return item


@router.post(
    '',
    tags=['regions_operation'],
    dependencies=[Depends(get_token_header)],
    status_code=201,
    response_model=models.Region,
)
async def create(region: models.RegionModify, session: Session = Depends(get_session)):
    try:
        item = CrudRegions.create(session, region.dict())
        return item
    except CrudException as e:
        raise HTTPException(status_code=400, detail=str(e))


@router.put(
    '/{region_id}',
    tags=['regions_operation'],
    dependencies=[Depends(get_token_header)],
    response_model=models.Region
)
async def update(region_id: int, region: models.RegionModify, session: Session = Depends(get_session)):
    try:
        item = CrudRegions.update(session, region_id, region.dict(exclude_unset=True))
        return item
    except CrudNotFoundException as e:
        raise HTTPException(status_code=404, detail=str(e))
    except CrudException as e:
        raise HTTPException(status_code=400, detail=str(e))


@router.delete(
    '/{region_id}',
    tags=['regions_operation'],
    status_code=204,
    dependencies=[Depends(get_token_header)]
)
async def delete(region_id: int, session: Session = Depends(get_session)):
    try:
        CrudRegions.delete(session, region_id)
    except CrudNotFoundException as e:
        raise HTTPException(status_code=404, detail=str(e))
    except CrudException as e:
        raise HTTPException(status_code=400, detail=str(e))
