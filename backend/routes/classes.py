from fastapi import APIRouter, Depends, HTTPException
from typing import List
from sqlalchemy.orm import Session
from backend import models
from backend.common.exceptions import CrudException, CrudNotFoundException
from backend.crud import CrudClasses
from backend.dependencies import get_session, get_token_header



router = APIRouter(
    prefix='/classes',
    tags=['classes'],
    responses={
        404: {
            'description': 'Method not found'
        }
    }
)


@router.get('', response_model=List[models.Class])
async def get_all(session: Session = Depends(get_session)):
    return CrudClasses.all(session)


@router.get('/{class_id}', response_model=models.Class)
async def get_by_id(class_id: int, session: Session = Depends(get_session)):
    item = CrudClasses.one(session, class_id)
    if not item:
        raise HTTPException(status_code=404, detail=f'Class with id = {class_id} not found')
    return item


@router.post(
    '',
    tags=['classes_operation'],
    dependencies=[Depends(get_token_header)],
    status_code=201,
    response_model=models.Class,
)
async def create(region: models.ClassModify, session: Session = Depends(get_session)):
    try:
        item = CrudClasses.create(session, region.dict())
        return item
    except CrudException as e:
        raise HTTPException(status_code=400, detail=str(e))


@router.put(
    '/{class_id}',
    tags=['classes_operation'],
    dependencies=[Depends(get_token_header)],
    response_model=models.Class
)
async def update(class_id: int, _class: models.ClassModify, session: Session = Depends(get_session)):
    try:
        item = CrudClasses.update(session, class_id, _class.dict(exclude_unset=True))
        return item
    except CrudNotFoundException as e:
        raise HTTPException(status_code=404, detail=str(e))
    except CrudException as e:
        raise HTTPException(status_code=400, detail=str(e))


@router.delete(
    '/{class_id}',
    tags=['classes_operation'],
    status_code=204,
    dependencies=[Depends(get_token_header)]
)
async def delete(class_id: int, session: Session = Depends(get_session)):
    try:
        CrudClasses.delete(session, class_id)
    except CrudNotFoundException as e:
        raise HTTPException(status_code=404, detail=str(e))
    except CrudException as e:
        raise HTTPException(status_code=400, detail=str(e))
