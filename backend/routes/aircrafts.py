from fastapi import APIRouter, Depends, HTTPException
from typing import List
from sqlalchemy.orm import Session
from backend import models
from backend.common.exceptions import CrudException, CrudNotFoundException
from backend.crud import CrudAircrafts
from backend.dependencies import get_session, get_token_header


router = APIRouter(
    prefix='/aircrafts',
    tags=['aircrafts'],
    responses={
        404: {
            'description': 'Method not found'
        }
    }
)


@router.get('', response_model=List[models.Aircraft])
async def get_all(session: Session = Depends(get_session)):
    return CrudAircrafts.all(session)


@router.get('/{aircraft_code}', response_model=models.Aircraft)
async def get_by_id(aircraft_code: str, session: Session = Depends(get_session)):
    item = CrudAircrafts.one(session, aircraft_code)
    if not item:
        raise HTTPException(status_code=404, detail=f'Aircraft with code = {aircraft_code} not found')
    return item


@router.post(
    '',
    tags=['aircrafts_operation'],
    dependencies=[Depends(get_token_header)],
    status_code=201,
    response_model=models.Aircraft,
)
async def create(aircraft: models.AircraftModify, session: Session = Depends(get_session)):
    try:
        item = CrudAircrafts.create(session, aircraft.dict())
        return item
    except CrudException as e:
        raise HTTPException(status_code=400, detail=str(e))
    except ValueError as e:
        raise HTTPException(status_code=400, detail=str(e))


@router.put(
    '/{code}',
    tags=['aircrafts_operation'],
    dependencies=[Depends(get_token_header)],
    response_model=models.Aircraft
)
async def update(code: str, aircraft: models.AircraftModify, session: Session = Depends(get_session)):
    try:
        item = CrudAircrafts.update(session, code, aircraft.dict(exclude_unset=True))
        return item
    except CrudNotFoundException as e:
        raise HTTPException(status_code=404, detail=str(e))
    except CrudException as e:
        raise HTTPException(status_code=400, detail=str(e))


@router.delete(
    '/{code}',
    tags=['classes_operation'],
    status_code=204,
    dependencies=[Depends(get_token_header)]
)
async def delete(code: str, session: Session = Depends(get_session)):
    try:
        CrudAircrafts.delete(session, code)
    except CrudNotFoundException as e:
        raise HTTPException(status_code=404, detail=str(e))
    except CrudException as e:
        raise HTTPException(status_code=400, detail=str(e))
