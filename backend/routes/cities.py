from fastapi import APIRouter, Depends, HTTPException
from typing import List
from sqlalchemy.orm import Session
from backend import models
from backend.common.exceptions import CrudException, CrudNotFoundException
from backend.crud import CrudCities
from backend.dependencies import get_session, get_token_header



router = APIRouter(
    prefix='/cities',
    tags=['cities'],
    responses={
        404: {
            'description': 'Method not found'
        }
    }
)


@router.get('', response_model=List[models.City])
async def get_all(session: Session = Depends(get_session), region_id: int = None):
    return CrudCities.all(session, region_id)


@router.get('/{city_id}', response_model=models.City)
async def get_by_id(city_id: int, session: Session = Depends(get_session)):
    return CrudCities.one(session, city_id)


@router.post(
    '',
    tags=['cities_operation'],
    dependencies=[Depends(get_token_header)],
    status_code=201,
    response_model=models.City,
)
async def create(city: models.CityInsert, session: Session = Depends(get_session)):
    try:
        item = CrudCities.create(session, city.dict())
        return item
    except CrudException as e:
        raise HTTPException(status_code=400, detail=str(e))


@router.put(
    '/{city_id}',
    tags=['cities_operation'],
    dependencies=[Depends(get_token_header)],
    response_model=models.City
)
async def update(city_id: int, city: models.CityUpdate, session: Session = Depends(get_session)):
    try:
        item = CrudCities.update(session, city_id, city.dict(exclude_unset=True))
        return item
    except CrudNotFoundException as e:
        raise HTTPException(status_code=404, detail=str(e))
    except CrudException as e:
        raise HTTPException(status_code=400, detail=str(e))


@router.delete(
    '/{city_id}',
    tags=['cities_operation'],
    status_code=204,
    dependencies=[Depends(get_token_header)]
)
async def delete(city_id: int, session: Session = Depends(get_session)):
    try:
        CrudCities.delete(session, city_id)
    except CrudNotFoundException as e:
        raise HTTPException(status_code=404, detail=str(e))
    except CrudException as e:
        raise HTTPException(status_code=400, detail=str(e))
