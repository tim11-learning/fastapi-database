from .regions import router as region_router
from .classes import router as class_router
from .aircrafts import router as aircrafts_router
from .seats import router as seats_router
from .cities import router as cities_router

routes = (
    region_router,
    class_router,
    aircrafts_router,
    seats_router,
    cities_router
)

__all__ = ['routes']
