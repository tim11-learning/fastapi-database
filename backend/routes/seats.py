from fastapi import APIRouter, Depends, HTTPException
from typing import List
from sqlalchemy.orm import Session
from backend import models
from backend.common.exceptions import CrudException, CrudNotFoundException
from backend.crud import CrudSeats
from backend.dependencies import get_session, get_token_header



router = APIRouter(
    prefix='/seats',
    tags=['seats'],
    responses={
        404: {
            'description': 'Method not found'
        }
    }
)


@router.get('', response_model=List[models.Seat])
async def get_all(session: Session = Depends(get_session)):
    return CrudSeats.all(session)


@router.get('/{aircraft_code}-{seat_no}', response_model=models.Seat)
async def get_one(aircraft_code: str, seat_no: str, session: Session = Depends(get_session)):
    item = CrudSeats.one(session, aircraft_code, seat_no)
    if not item:
        raise HTTPException(
            status_code=404,
            detail=f'Seat in aircraft {aircraft_code} with seat_no = {seat_no} not found'
        )
    return item

@router.get('/{aircraft_code}', response_model=List[models.Seat])
async def get_all_by_aircraft(aircraft_code: str, session: Session = Depends(get_session)):
    return CrudSeats.all(session, aircraft_code)


@router.post(
    '',
    tags=['seats_operation'],
    dependencies=[Depends(get_token_header)],
    status_code=201,
    response_model=models.Seat,
)
async def create(seat: models.SeatInsert, session: Session = Depends(get_session)):
    try:
        item = CrudSeats.create(session, seat.dict())
        return item
    except CrudException as e:
        raise HTTPException(status_code=400, detail=str(e))


@router.put(
    '/{aircraft_code}-{seat_no}',
    tags=['seats_operation'],
    dependencies=[Depends(get_token_header)],
    response_model=models.Seat
)
async def update(aircraft_code: str, seat_no: str, seat: models.SeatUpdate, session: Session = Depends(get_session)):
    try:
        item = CrudSeats.update(session, aircraft_code, seat_no, seat.dict(exclude_unset=True))
        return item
    except CrudNotFoundException as e:
        raise HTTPException(status_code=404, detail=str(e))
    except CrudException as e:
        raise HTTPException(status_code=400, detail=str(e))


@router.delete(
    '/{aircraft_code}-{seat_no}',
    tags=['seats_operation'],
    status_code=204,
    dependencies=[Depends(get_token_header)]
)
async def delete(aircraft_code: str, seat_no: str, session: Session = Depends(get_session)):
    try:
        CrudSeats.delete(session, aircraft_code, seat_no)
    except CrudNotFoundException as e:
        raise HTTPException(status_code=404, detail=str(e))
    except CrudException as e:
        raise HTTPException(status_code=400, detail=str(e))
