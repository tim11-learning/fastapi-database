from .base import Base
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship


class Seats(Base):
    __tablename__: str = 'seats'

    aircraft_code = Column(String(3), primary_key=True, index=True, comment='Код самолета, IATA')
    seat_no = Column(String(4), primary_key=True, index=True, nullable=False, comment='Номер места')
    fare_conditions = Column(Integer, ForeignKey('classes.id'), comment='Идентификатор класса обслуживания')
    class_info = relationship('Classes')
    aircraft_info = relationship('Aircrafts', backref='seats',
                                 foreign_keys=aircraft_code,
                                 primaryjoin='Aircrafts.aircraft_code == Seats.aircraft_code')

