from .base import Base
from sqlalchemy import Column, Integer, String, ForeignKey, UniqueConstraint, Boolean
from sqlalchemy.orm import relationship


class Cities(Base):
    __tablename__: str = 'cities'

    id = Column(Integer, primary_key=True, index=True, autoincrement=True, comment='Идентификатор города')
    region_id = Column(Integer, ForeignKey('regions.id'), nullable=False, comment='Идентификатор региона')
    name = Column(String, index=True, comment='Название города')
    region_info = relationship('Regions', back_populates="cities")
    deleted = Column(Boolean, default=False, comment='Помечен как удаленный')


UniqueConstraint(Cities.region_id, Cities.name)
