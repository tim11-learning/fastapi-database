from .base import Base
from .regions import Regions
from .classes import Classes
from .aircrafts import Aircrafts
from .seats import Seats
from .cities import Cities

__all__ = ['Base', 'Regions', 'Classes', 'Aircrafts', 'Seats', 'Cities']
