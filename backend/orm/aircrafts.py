from .base import Base
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import validates


class Aircrafts(Base):
    __tablename__: str = 'aircrafts'

    aircraft_code = Column(String(3), primary_key=True, index=True, comment='Код самолета, IATA', nullable=False)
    model = Column(String, nullable=False, index=True, unique=True, comment='Модель самолета')
    distance = Column(Integer, nullable=False, comment='Максимальная дальность полета, км')

    @validates('distance')
    def validate_distance(self, key: str, distance: int) -> ValueError | int:
        if distance <= 0:
            raise ValueError('distance must have a value greater than 0')
        return distance
