from .base import Base
from .cities import Cities
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship


class Regions(Base):
    __tablename__: str = 'regions'

    id = Column(Integer, primary_key=True, index=True, autoincrement=True, comment='Идентификатор региона')
    name = Column(String, unique=True, index=True, comment='Наименование региона')


Regions.cities = relationship("Cities", order_by = Cities.id, back_populates = "region_info")
