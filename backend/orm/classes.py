from .base import Base
from sqlalchemy import Column, Integer, String


class Classes(Base):
    __tablename__: str = 'classes'

    id = Column(Integer, primary_key=True, index=True, autoincrement=True, comment='Идентификатор класса обслуживания')
    name = Column(String, unique=True, index=True, comment='Название класса обслуживания')
