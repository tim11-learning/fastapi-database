from pydantic import BaseModel
from .base import Base, OrmBase
from typing import List


class RegionBase(BaseModel):
    name: str


class RegionModify(RegionBase):
    pass


class RegionRelation(RegionBase, OrmBase):
    id: int


class Region(RegionRelation, Base):
    from .cities import CityRelation
    cities: List[CityRelation]
