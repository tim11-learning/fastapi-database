from pydantic import BaseModel
from .base import Base, OrmBase


class ClassBase(BaseModel):
    name: str


class ClassModify(ClassBase):
    pass


class ClassRelation(ClassBase, OrmBase):
    id: int


class Class(ClassRelation, Base):
    pass
