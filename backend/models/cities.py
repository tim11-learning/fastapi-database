from pydantic import BaseModel
from pydantic.class_validators import Optional

from .regions import RegionRelation
from .base import Base, OrmBase


class CityBase(BaseModel):
    name: str


class CityInsert(CityBase):
    region_id: int


class CityUpdate(BaseModel):
    name: Optional[str] = None
    region_id: Optional[int] = None


class City(CityBase, Base, OrmBase):
    id: int
    region_info: RegionRelation


class CityRelation(CityBase, OrmBase):
    id: int
