from pydantic import BaseModel
from .base import Base, OrmBase


class AircraftBase(BaseModel):
    aircraft_code: str
    model: str
    distance: int


class AircraftModify(AircraftBase):
    pass


class Aircraft(AircraftBase, Base, OrmBase):
    pass

class AircraftRelation(AircraftBase, OrmBase):
    pass
