from pydantic import BaseModel
from pydantic.class_validators import Optional

from .classes import ClassRelation
from .aircrafts import AircraftRelation
from .base import Base, OrmBase


class SeatBase(BaseModel):
    aircraft_code: str
    seat_no: str


class SeatInsert(SeatBase):
    fare_conditions: int


class SeatUpdate(BaseModel):
    aircraft_code: Optional[str] = None
    seat_no: Optional[str] = None
    fare_conditions: Optional[int] = None


class Seat(SeatBase, Base, OrmBase):
    class_info: ClassRelation
    aircraft_info: AircraftRelation
