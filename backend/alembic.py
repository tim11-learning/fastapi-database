import sys
sys.path.insert(1, '../')
import argparse
import os
import shutil
import configparser
import re
from backend.common import Config


MIGRATION_FOLDER_NAME = 'migrations'
MIGRATION_FOLDER_PATH = os.path.abspath('./' + MIGRATION_FOLDER_NAME)
ALEMBIC_ORIGINAL_FILE = os.path.abspath('./alembic.ini')
ALEMBIC_TEMPLATE_FILE = os.path.abspath('./default.alembic.ini')
ENV_FILE = os.path.abspath('./.env')


def exists(name: str, as_dir: bool = False) -> bool:
    return os.path.exists(name) and (os.path.isdir(name) if as_dir else os.path.isfile(name))


def action_init():
    print('Run init')
    if exists(MIGRATION_FOLDER_PATH, True):
        return print('Migration folder is exists')
    try:
        os.system(f'alembic init {MIGRATION_FOLDER_NAME}')
        print('Migration folder created')
    except BaseException as e:
        print(e)


def action_config():
    print('Run config')
    if not exists(ALEMBIC_TEMPLATE_FILE) and exists(ALEMBIC_ORIGINAL_FILE):
        shutil.move(ALEMBIC_ORIGINAL_FILE, ALEMBIC_TEMPLATE_FILE)
    if not exists(ALEMBIC_TEMPLATE_FILE):
        return
    config = configparser.ConfigParser()
    config.read(ALEMBIC_TEMPLATE_FILE)

    base_config = Config(ENV_FILE)
    config['alembic']['sqlalchemy.url'] = base_config.dsn
    with open(ALEMBIC_ORIGINAL_FILE, 'w') as configfile:
        config.write(configfile)


def action_revision():
    print('Run revision')
    comment = ''
    while not comment:
        comment = input('Введите название миграции: ')
        comment = re.sub('[^a-zA-z0-9_ ]+', '', comment).strip()
    try:
        os.system(f'alembic revision --autogenerate -m "{comment}"')
    except BaseException as e:
        print(e)


def action_up():
    print('Run migration up')
    try:
        os.system(f'alembic upgrade heads')
    except BaseException as e:
        print(e)


parser = argparse.ArgumentParser(description='Скрипт для работы с миграциями')
parser.add_argument(
    '-i', '--init', help='Инициализация системы миграций', action='store_true',
)
parser.add_argument(
    '-c', '--config', help='Генерация alembic.ini', action='store_true'
)
parser.add_argument(
    '-r', '--revision', help='Создание миграции', action='store_true'
)
parser.add_argument(
    '--up', help='Применение миграции', action='store_true'
)

args = parser.parse_args()
if args.init:
    action_init()
if args.config:
    action_config()
if args.revision:
    action_revision()
if args.up:
    action_up()
