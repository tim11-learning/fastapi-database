init: docker-down-clear frontend-clear docker-pull docker-build docker-up migration-config frontend-init
down: docker-down-clear frontend-clear
lint: frontend-lint

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

frontend-clear:
	docker run --rm -v ${PWD}/web:/app -w /app alpine sh -c 'rm -rf .ready build'

frontend-init: frontend-yarn-install frontend-ready

frontend-yarn-install:
	docker-compose run --rm frontend-node-cli yarn install

frontend-ready:
	docker run --rm -v ${PWD}/web:/app -w /app alpine touch .ready

frontend-lint:
	docker-compose run --rm frontend-node-cli yarn eslint
	docker-compose run --rm frontend-node-cli yarn stylelint

frontend-prettier:
	docker-compose run --rm frontend-node-cli yarn prettier

frontend-lint-fix:
	docker-compose run --rm frontend-node-cli yarn eslint-fix

migration-init:
	docker-compose run --rm backend-python python alembic.py --init --config

migration-config:
	docker-compose run --rm backend-python python alembic.py --config

migration-create:
	docker-compose run --rm backend-python python alembic.py --revision

migration-up:
	docker-compose run --rm backend-python python alembic.py --up
