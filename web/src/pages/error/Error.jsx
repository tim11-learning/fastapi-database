import React from 'react'
import Message from '../../components/message/Message'
import { useRouteError } from 'react-router-dom'

function Error() {
  const error = useRouteError()
  return (
    <Message
      title={error.status || 'Ooops'}
      message={error.statusText || error.message}
      style="error"
    />
  )
}

export default Error
