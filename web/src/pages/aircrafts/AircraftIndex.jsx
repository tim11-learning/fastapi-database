import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import api from '../../api'
import Breadcrumbs from '../../components/breadcrumbs/Breadcrumbs'
import Loader from '../../components/loader/Loader'

function AircraftIndex() {
  const [data, setData] = useState([])
  const [loaded, setLoaded] = useState(false)
  useEffect(() => {
    api.aircrafts.list().then((response) => {
      setLoaded(true)
      if (response.ok) return setData(response.data)
    })
  }, [])
  return (
    <>
      <Breadcrumbs links={[{ title: 'Список самолетов' }]} />
      <h1>Список самолетов</h1>
      <p>
        <Link to="/aircrafts/add" className="btn btn-success">
          Добавить самолет
        </Link>
      </p>
      {!loaded && <Loader />}
      {loaded &&
        (() => {
          if (!data.length)
            return (
              <div className="alert alert-warning">Список самолетов пуст</div>
            )
          return (
            <table className="table table-bordered table-sm">
              <thead className="thead-dark">
                <tr>
                  <th scope="col">Код самолета, IATA</th>
                  <th scope="col">Модель самолета</th>
                  <th scope="col">Максимальная дальность полета, км</th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody>
                {(data || []).map((item) => {
                  return (
                    <React.Fragment key={'aircraft-' + item.aircraft_code}>
                      <tr>
                        <th scope="row">{item.aircraft_code}</th>
                        <td>
                          <Link to={`/aircrafts/${item.aircraft_code}`}>
                            {item.model}
                          </Link>
                        </td>
                        <td>{item.distance}</td>
                        <td
                          className="text-right"
                          style={{
                            width: '1%',
                            paddingLeft: '15px',
                            paddingRight: '15px',
                            whiteSpace: 'nowrap',
                          }}
                        >
                          <Link
                            to={`/aircrafts/${item.aircraft_code}/edit`}
                            title="Изменить"
                          >
                            <i className="fa-solid fa-pen-to-square" />
                          </Link>
                          &nbsp;
                          <Link
                            to={`/aircrafts/${item.aircraft_code}/delete`}
                            title="Удалить"
                            className="text-danger"
                          >
                            <i className="fa-regular fa-circle-xmark" />
                          </Link>
                        </td>
                      </tr>
                    </React.Fragment>
                  )
                })}
              </tbody>
            </table>
          )
        })()}
    </>
  )
}

export default AircraftIndex
