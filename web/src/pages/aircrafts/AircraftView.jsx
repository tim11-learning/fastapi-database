import React, { useState, useEffect } from 'react'
import { useParams, Link } from 'react-router-dom'
import api from '../../api'
import Message from '../../components/message/Message'
import Breadcrumbs from '../../components/breadcrumbs/Breadcrumbs'
import Loader from '../../components/loader/Loader'

function AircraftView() {
  const { aircraftId: code } = useParams()
  const [loaded, setLoaded] = useState(false)
  const [data, setData] = useState(null)
  const [error, setError] = useState(false)
  useEffect(() => {
    if (loaded) return
    api.aircrafts
      .view(code)
      .then((response) => {
        if (response.ok) return setData(response.data)
        throw response
      })
      .catch((e) => {
        setError(
          <Message
            title={e.code || e.statusCode}
            message={e.error || e.statusText || e.message}
          />
        )
      })
      .finally(() => setLoaded(true))
  }, [])
  if (error) return error
  return (
    <>
      {!loaded && <Loader />}
      {data && (
        <React.Fragment>
          <Breadcrumbs
            links={[
              { title: 'Список самолетов', link: '/aircrafts' },
              { title: `${data.model} (${data.aircraft_code})` },
            ]}
          />
          <h1>{`${data.model} (${data.aircraft_code})`}</h1>
          <table className="table table-sm table-bordered">
            <tbody>
              <tr>
                <th>Код самолета, IATA</th>
                <td>{data.aircraft_code}</td>
              </tr>
              <tr>
                <th>Модель самолета</th>
                <td>{data.model}</td>
              </tr>
              <tr>
                <th>Максимальная дальность полета</th>
                <td>{data.distance} км</td>
              </tr>
            </tbody>
          </table>
          <div
            className="btn-group"
            role="group"
            aria-label="Управление самолетами"
          >
            <Link
              to={`/aircrafts/${data.aircraft_code}/edit`}
              className="btn btn-warning"
            >
              Изменить
            </Link>
            <Link
              to={`/aircrafts/${data.aircraft_code}/delete`}
              className="btn btn-danger"
            >
              Удалить
            </Link>
          </div>
        </React.Fragment>
      )}
    </>
  )
}

export default AircraftView
