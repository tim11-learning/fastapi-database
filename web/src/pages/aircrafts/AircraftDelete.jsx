import React, { useState, useEffect } from 'react'
import { useParams, Link, useNavigate } from 'react-router-dom'
import api from '../../api'
import Message from '../../components/message/Message'
import Breadcrumbs from '../../components/breadcrumbs/Breadcrumbs'
import Loader from '../../components/loader/Loader'

function AircraftDelete() {
  const navigate = useNavigate()
  const { aircraftId: code } = useParams()
  const [loaded, setLoaded] = useState(false)
  const [data, setData] = useState()
  const [error, setError] = useState(false)
  useEffect(() => {
    if (loaded) return
    api.aircrafts
      .view(code)
      .then((response) => {
        if (response.ok) return setData(response.data)
        throw response
      })
      .catch((e) => {
        setError(
          <Message
            title={e.code || e.statusCode}
            message={e.error || e.statusText || e.message}
          />
        )
      })
      .finally(() => {
        setLoaded(true)
      })
  }, [])
  if (error) return error
  const handleConfirm = (e) => {
    e.preventDefault()
    api.aircrafts
      .delete(code)
      .then((response) => {
        if (!response.ok) throw response
        navigate(`/aircrafts`)
      })
      .catch((response) => {
        alert(response.error)
      })
  }
  return (
    <div>
      {!loaded && <Loader />}
      {loaded && data && (
        <Breadcrumbs
          links={[
            { title: 'Список самолетов', link: '/aircrafts' },
            {
              title: `${data.model} (${data.aircraft_code})`,
              link: '/aircrafts/' + data.id,
            },
            { title: 'Удаление самолета' },
          ]}
        />
      )}
      <h1>Удаление самолета</h1>
      {!loaded && <Loader />}
      {loaded && (
        <React.Fragment>
          <div className="alert alert-warning">
            Вы действительно хотите удалить класс обслуживания &nbsp;
            <b>{`${data.model} (${data.aircraft_code})`}</b>?
          </div>
          <Link to={'/aircrafts'} className="btn btn-secondary">
            Нет
          </Link>
          &nbsp;
          <button className="btn btn-danger" onClick={handleConfirm}>
            Да
          </button>
        </React.Fragment>
      )}
    </div>
  )
}

export default AircraftDelete
