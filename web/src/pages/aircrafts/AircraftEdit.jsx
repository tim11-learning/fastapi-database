import React, { useState, useEffect } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import api from '../../api'
import Message from '../../components/message/Message'
import Breadcrumbs from '../../components/breadcrumbs/Breadcrumbs'
import Loader from '../../components/loader/Loader'

function AircraftEdit() {
  const navigate = useNavigate()
  const { aircraftId: code } = useParams()
  const [loaded, setLoaded] = useState(false)
  const [aircraftCode, setAircraftCode] = useState()
  const [model, setModel] = useState()
  const [distance, setDistance] = useState()
  const [data, setData] = useState()
  const [error, setError] = useState(false)
  const [except, setExcept] = useState(false)
  const [disabled, setDisabled] = useState(false)
  useEffect(() => {
    if (!code) return setLoaded(true)
    if (loaded) return
    api.aircrafts
      .view(code)
      .then((response) => {
        if (response.ok) {
          const d = response.data
          setData(d)
          setAircraftCode(d.aircraft_code)
          setModel(d.model)
          setDistance(d.distance)
          return
        }
        throw response
      })
      .catch((e) => {
        setExcept(
          <Message
            title={e.code || e.statusCode}
            message={e.error || e.statusText || e.message}
          />
        )
      })
      .finally(() => {
        setLoaded(true)
      })
  }, [])
  if (except) return except

  const validate = () => {
    const aC = (aircraftCode || '').toString().trim()
    const aM = (model || '').toString().trim()
    const aD = Number(distance)
    return aC && aM && /\d+/.test(aD)
  }
  const handleSubmit = (e) => {
    e.preventDefault()
    if (!validate()) return
    setDisabled(true)
    const sendData = {
      aircraft_code: aircraftCode,
      model,
      distance,
    }
    const promise = code
      ? api.aircrafts.update(code, sendData)
      : api.aircrafts.create(sendData)
    promise
      .then((response) => {
        if (!response.ok) throw response
        navigate(`/aircrafts/${response.data.aircraft_code}`)
      })
      .catch((response) => {
        setError(response.error)
      })
      .finally(() => {
        setDisabled(false)
      })
  }
  const links = [{ title: 'Список самолетов', link: '/aircrafts' }]
  if (code && data) {
    links.push({
      title: data ? `${data.model} (${data.aircraft_code})` : '',
      link: '/aircrafts/' + code,
    })
    links.push({ title: 'Изменение самолета' })
  } else if (!code) {
    links.push({ title: 'Добавление самолета' })
  }
  return (
    <>
      <Breadcrumbs links={links} />
      <h1>{!code ? 'Добавление самолета' : 'Изменение самолета'}</h1>
      {!loaded && <Loader />}
      {loaded && (
        <form onSubmit={handleSubmit}>
          {error && <div className="alert alert-danger">{error}</div>}
          <div className="form-group">
            <label htmlFor="aircraft-code">Код самолета, IATA</label>
            <input
              className="form-control"
              type="text"
              name="aircraft_code"
              id="aircraft-code"
              value={aircraftCode}
              maxLength="3"
              placeholder="Введите код самолета, IATA"
              onChange={(e) => setAircraftCode(e.target.value)}
            />
          </div>
          <div className="form-group">
            <label htmlFor="aircraft-model">Модель самолета</label>
            <input
              className="form-control"
              type="text"
              name="model"
              id="aircraft-model"
              value={model}
              placeholder="Введите название модель самолета"
              onChange={(e) => setModel(e.target.value)}
            />
          </div>
          <div className="form-group">
            <label htmlFor="aircraft-distance">
              Максимальная дальность полета, км
            </label>
            <input
              className="form-control"
              type="text"
              name="distance"
              id="aircraft-distance"
              value={distance}
              maxLength="10"
              placeholder="Введите максимальную дальность полета, км"
              onChange={(e) =>
                setDistance(+e.target.value.replace(/[^\d,]/g, ''))
              }
            />
          </div>
          <button
            type="submit"
            className="btn btn-primary"
            disabled={disabled || !validate()}
          >
            {code ? 'Сохранить' : 'Добавить'}
          </button>
        </form>
      )}
    </>
  )
}

export default AircraftEdit
