import React, { useState, useEffect } from 'react'
import { useParams, Link, useNavigate } from 'react-router-dom'
import api from '../../api'
import Message from '../../components/message/Message'
import Breadcrumbs from '../../components/breadcrumbs/Breadcrumbs'
import Loader from '../../components/loader/Loader'

function ClassDelete() {
  const navigate = useNavigate()
  const { classId: id } = useParams()
  const [loaded, setLoaded] = useState(false)
  const [data, setData] = useState()
  const [error, setError] = useState(false)
  useEffect(() => {
    if (loaded) return
    api.classes
      .view(id)
      .then((response) => {
        if (response.ok) return setData(response.data)
        throw response
      })
      .catch((e) => {
        setError(
          <Message
            title={e.code || e.statusCode}
            message={e.error || e.statusText || e.message}
          />
        )
      })
      .finally(() => {
        setLoaded(true)
      })
  }, [])
  if (error) return error
  const handleConfirm = (e) => {
    e.preventDefault()
    api.classes
      .delete(id)
      .then((response) => {
        if (!response.ok) throw response
        navigate(`/classes`)
      })
      .catch((response) => {
        alert(response.error)
      })
  }
  return (
    <div>
      {!loaded && <Loader />}
      {loaded && data && (
        <Breadcrumbs
          links={[
            { title: 'Классы обслуживания', link: '/classes' },
            { title: data.name, link: '/classes/' + data.id },
            { title: 'Удаление класса обслуживания' },
          ]}
        />
      )}
      <h1>Удаление класса обслуживания</h1>
      {!loaded && <Loader />}
      {loaded && (
        <React.Fragment>
          <div className="alert alert-warning">
            Вы действительно хотите удалить класс обслуживания &nbsp;
            <b>{data.name}</b>?
          </div>
          <Link to={'/classes'} className="btn btn-secondary">
            Нет
          </Link>
          &nbsp;
          <button className="btn btn-danger" onClick={handleConfirm}>
            Да
          </button>
        </React.Fragment>
      )}
    </div>
  )
}

export default ClassDelete
