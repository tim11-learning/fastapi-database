import React, { useState, useEffect } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import api from '../../api'
import Message from '../../components/message/Message'
import Breadcrumbs from '../../components/breadcrumbs/Breadcrumbs'
import Loader from '../../components/loader/Loader'

function ClassEdit() {
  const navigate = useNavigate()
  const { classId: id } = useParams()
  const [loaded, setLoaded] = useState(false)
  const [name, setName] = useState(null)
  const [data, setData] = useState()
  const [error, setError] = useState(false)
  const [except, setExcept] = useState(false)
  const [disabled, setDisabled] = useState(false)
  useEffect(() => {
    if (!id) return setLoaded(true)
    if (loaded) return
    api.classes
      .view(id)
      .then((response) => {
        if (response.ok) {
          setData(response.data)
          return setName(response.data.name)
        }
        throw response
      })
      .catch((e) => {
        setExcept(
          <Message
            title={e.code || e.statusCode}
            message={e.error || e.statusText || e.message}
          />
        )
      })
      .finally(() => {
        setLoaded(true)
      })
  }, [])
  if (except) return except

  const validate = () => {
    return (name || '').toString().trim()
  }
  const handleSubmit = (e) => {
    e.preventDefault()
    if (!validate()) return
    setDisabled(true)
    const sendData = { name: name.toString().trim() }
    const promise = id
      ? api.classes.update(id, sendData)
      : api.classes.create(sendData)
    promise
      .then((response) => {
        if (!response.ok) throw response
        navigate(`/classes/${response.data.id}`)
      })
      .catch((response) => {
        setError(response.error)
      })
      .finally(() => {
        setDisabled(false)
      })
  }
  const links = [{ title: 'Классы обслужинивая', link: '/classes' }]
  if (id && data) {
    links.push({ title: data ? data.name : '', link: '/classes/' + id })
    links.push({ title: 'Изменение класса обслужинивая' })
  } else if (!id) {
    links.push({ title: 'Добавление класса обслужинивая' })
  }
  return (
    <>
      <Breadcrumbs links={links} />
      <h1>
        {!id
          ? 'Добавление класса обслужинивая'
          : 'Изменение класса обслужинивая'}
      </h1>
      {!loaded && <Loader />}
      {loaded && (
        <form onSubmit={handleSubmit}>
          {error && <div className="alert alert-danger">{error}</div>}
          <div className="form-group">
            <label htmlFor="class-name">Название класса обсуживания</label>
            <input
              className="form-control"
              type="text"
              name="name"
              id="class-name"
              value={name}
              placeholder="Введите название класса обсуживания"
              onChange={(e) => setName(e.target.value)}
            />
          </div>
          <button
            type="submit"
            className="btn btn-primary"
            disabled={disabled || !validate()}
          >
            {id ? 'Сохранить' : 'Добавить'}
          </button>
        </form>
      )}
    </>
  )
}

export default ClassEdit
