import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import api from '../../api'
import Breadcrumbs from '../../components/breadcrumbs/Breadcrumbs'
import Loader from '../../components/loader/Loader'

function RegionIndex() {
  const [data, setData] = useState([])
  const [loaded, setLoaded] = useState(false)
  useEffect(() => {
    api.cities.list().then((response) => {
      setLoaded(true)
      if (response.ok) return setData(response.data)
    })
  }, [])
  return (
    <>
      <Breadcrumbs links={[{ title: 'Города' }]} />
      <h1>Список городов</h1>
      <p>
        <Link to="/cities/add" className="btn btn-success">
          Добавить город
        </Link>
      </p>
      {!loaded && <Loader />}
      {loaded &&
        (() => {
          if (!data.length)
            return (
              <div className="alert alert-warning">Список городов пуст</div>
            )
          return (
            <table className="table table-bordered table-sm">
              <thead className="thead-dark">
                <tr>
                  <th scope="col" className="text-center">
                    Идентификатор
                  </th>
                  <th scope="col">Название города</th>
                  <th scope="col">Регион</th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody>
                {(data || []).map((item) => {
                  return (
                    <React.Fragment key={'city-' + item.id}>
                      <tr>
                        <th
                          scope="row"
                          className="text-center"
                          style={{
                            width: '1%',
                            paddingLeft: '15px',
                            paddingRight: '15px',
                          }}
                        >
                          {item.id}
                        </th>
                        <td>
                          <Link to={`/cities/${item.id}`}>{item.name}</Link>
                        </td>
                        <td>{item.region_info.name}</td>
                        <td
                          className="text-right"
                          style={{
                            width: '1%',
                            paddingLeft: '15px',
                            paddingRight: '15px',
                            whiteSpace: 'nowrap',
                          }}
                        >
                          <Link to={`/cities/${item.id}/edit`} title="Изменить">
                            <i className="fa-solid fa-pen-to-square" />
                          </Link>
                          &nbsp;
                          <Link
                            to={`/cities/${item.id}/delete`}
                            title="Удалить"
                            className="text-danger"
                          >
                            <i className="fa-regular fa-circle-xmark" />
                          </Link>
                        </td>
                      </tr>
                    </React.Fragment>
                  )
                })}
              </tbody>
            </table>
          )
        })()}
    </>
  )
}

export default RegionIndex
