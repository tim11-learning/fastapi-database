import React, { useState, useEffect } from 'react'
import { useParams, Link, useNavigate } from 'react-router-dom'
import api from '../../api'
import Message from '../../components/message/Message'
import Breadcrumbs from '../../components/breadcrumbs/Breadcrumbs'
import Loader from '../../components/loader/Loader'

function CityDelete() {
  const navigate = useNavigate()
  const { cityId: id } = useParams()
  const [loaded, setLoaded] = useState(false)
  const [data, setData] = useState()
  const [error, setError] = useState(false)
  useEffect(() => {
    if (loaded) return
    api.cities
      .view(id)
      .then((response) => {
        if (response.ok) return setData(response.data)
        throw response
      })
      .catch((e) => {
        setError(
          <Message
            title={e.code || e.statusCode}
            message={e.error || e.statusText || e.message}
          />
        )
      })
      .finally(() => {
        setLoaded(true)
      })
  }, [])
  if (error) return error
  const handleConfirm = (e) => {
    e.preventDefault()
    api.cities
      .delete(id)
      .then((response) => {
        if (!response.ok) throw response
        navigate(`/cities`)
      })
      .catch((response) => {
        alert(response.error)
      })
  }
  return (
    <div>
      {!data && <Loader />}
      {data && (
        <Breadcrumbs
          links={[
            { title: 'Города', link: '/cities' },
            { title: data.name, link: '/cities/' + data.id },
            { title: 'Удаление города' },
          ]}
        />
      )}
      <h1>Удаление города</h1>
      {!loaded && <Loader />}
      {loaded && (
        <React.Fragment>
          <div className="alert alert-warning">
            Вы действительно хотите удалить город{' '}
            <b>
              {data.name} ({data.region_info.name})
            </b>
            ?
          </div>
          <Link to={'/cities'} className="btn btn-secondary">
            Нет
          </Link>
          &nbsp;
          <button className="btn btn-danger" onClick={handleConfirm}>
            Да
          </button>
        </React.Fragment>
      )}
    </div>
  )
}

export default CityDelete
