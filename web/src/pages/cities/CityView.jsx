import React, { useState, useEffect } from 'react'
import { useParams, Link } from 'react-router-dom'
import api from '../../api'
import Message from '../../components/message/Message'
import Breadcrumbs from '../../components/breadcrumbs/Breadcrumbs'
import Loader from '../../components/loader/Loader'

function CityView() {
  const { cityId: id } = useParams()
  const [loaded, setLoaded] = useState(false)
  const [data, setData] = useState(null)
  const [error, setError] = useState(false)
  useEffect(() => {
    if (loaded) return
    api.cities
      .view(id)
      .then((response) => {
        if (response.ok) return setData(response.data)
        throw response
      })
      .catch((e) => {
        setLoaded(true)
        setError(
          <Message
            title={e.code || e.statusCode}
            message={e.error || e.statusText || e.message}
          />
        )
      })
      .finally(() => {
        setLoaded(true)
      })
  }, [])
  if (error) return error
  return (
    <>
      {!loaded && <Loader />}
      {loaded && (
        <React.Fragment>
          <Breadcrumbs
            links={[{ title: 'Города', link: '/cities' }, { title: data.name }]}
          />
          <h1>{data.name}</h1>
          <table className="table table-sm table-bordered">
            <tbody>
              <tr>
                <th>Название города</th>
                <td>{data.name}</td>
              </tr>
              <tr>
                <th>Регион</th>
                <td>{data.region_info.name}</td>
              </tr>
            </tbody>
          </table>
          <div
            className="btn-group"
            role="group"
            aria-label="Управление городами"
          >
            <Link to={`/cities/${data.id}/edit`} className="btn btn-warning">
              Изменить
            </Link>
            <Link to={`/cities/${data.id}/delete`} className="btn btn-danger">
              Удалить
            </Link>
          </div>
        </React.Fragment>
      )}
    </>
  )
}

export default CityView
