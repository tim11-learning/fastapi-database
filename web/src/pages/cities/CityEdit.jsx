import React, { useState, useEffect } from 'react'
import { useParams, useNavigate, Link } from 'react-router-dom'
import api from '../../api'
import Message from '../../components/message/Message'
import Breadcrumbs from '../../components/breadcrumbs/Breadcrumbs'
import Loader from '../../components/loader/Loader'

function CityEdit() {
  const navigate = useNavigate()
  const { cityId: id } = useParams()
  const [loaded, setLoaded] = useState(false)
  const [regions, setRegions] = useState([])
  const [data, setData] = useState()
  const [name, setName] = useState('')
  const [regionId, setRegionId] = useState('')
  const [error, setError] = useState(false)
  const [except, setExcept] = useState(false)
  const [disabled, setDisabled] = useState(false)
  useEffect(() => {
    if (loaded) return
    const promises = [api.regions.list()]
    if (id) {
      promises.push(api.cities.view(id))
    }
    Promise.all(promises)
      .then((values) => {
        if (!values[0].ok) throw values[0]
        setRegions(
          (values[0].data || []).map((item) => {
            return { id: item.id, name: item.name }
          })
        )
        if (!id) return
        if (!values[1].ok) throw values[1]
        const d = values[1].data
        setName(d.name)
        setRegionId(d.region_info.id)
        setData(d)
      })
      .catch((e) => {
        setExcept(
          <Message
            title={e.code || e.statusCode}
            message={e.error || e.statusText || e.message}
          />
        )
      })
      .finally(() => {
        setLoaded(true)
      })
  })

  const links = [{ title: 'Города', link: '/cities' }]
  if (id && data) {
    links.push({ title: data.name, link: '/cities/' + id })
    links.push({ title: 'Изменение города' })
  } else if (!id) {
    links.push({ title: 'Добавление города' })
  }
  if (except) return except

  const validate = () => {
    return !!regionId && (name || '').toString().trim()
  }
  const handleSubmit = (e) => {
    e.preventDefault()
    if (!validate()) return
    setDisabled(true)
    const sendData = { name: name.trim(), region_id: regionId }
    console.log(sendData)
    const promise = id
      ? api.cities.update(id, sendData)
      : api.cities.create(sendData)
    promise
      .then((response) => {
        if (!response.ok) throw response
        navigate(`/cities/${response.data.id}`)
      })
      .catch((response) => {
        setError(response.error)
      })
      .finally(() => {
        setDisabled(false)
      })
  }
  return (
    <>
      <Breadcrumbs links={links} />
      <h1>{!id ? 'Добавление города' : 'Изменение города'}</h1>
      {!loaded && <Loader />}
      {loaded &&
        (() => {
          if (!regions.length) {
            return (
              <div className="alert alert-warning">
                <p>
                  Перед добавлением города Вам необходимо добавить хотя бы один
                  регион
                </p>
                <Link to={'/regions/add'} className="btn btn-success">
                  Добавить
                </Link>
              </div>
            )
          }
          return (
            <form onSubmit={handleSubmit}>
              {error && <div className="alert alert-danger">{error}</div>}
              <div className="form-group">
                <label htmlFor="city-name">Название города</label>
                <input
                  className="form-control"
                  type="text"
                  name="name"
                  id="city-name"
                  value={name}
                  placeholder="Введите название города"
                  onChange={(e) => setName(e.target.value)}
                />
              </div>
              <div className="form-group">
                <label htmlFor="city-region">Регион</label>
                <select
                  className="form-control"
                  id="city-region"
                  name="regionId"
                  value={regionId}
                  onChange={(e) => setRegionId(e.target.value)}
                >
                  <option value="">Выберите регион</option>
                  {regions.map((region, index) => {
                    return (
                      <React.Fragment key={'city-region-' + region.id}>
                        <option value={region.id}>{region.name}</option>
                      </React.Fragment>
                    )
                  })}
                </select>
              </div>
              <button
                type="submit"
                className="btn btn-primary"
                disabled={disabled || !validate()}
              >
                {id ? 'Сохранить' : 'Добавить'}
              </button>
            </form>
          )
        })()}
    </>
  )

  // if (id) {
  //   useEffect(() => {
  //     api.regions
  //       .view(id)
  //       .then((response) => {
  //         if (response.ok) {
  //           return setName(response.data.name)
  //         }
  //         throw response
  //       })
  //       .catch((e) => {
  //         setExcept(
  //           <Message
  //             title={e.code || e.statusCode}
  //             message={e.error || e.statusText || e.message}
  //           />
  //         )
  //       })
  //   }, [])
  // }
}

export default CityEdit
