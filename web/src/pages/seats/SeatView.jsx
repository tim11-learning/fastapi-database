import React, { useState, useEffect } from 'react'
import { useParams, Link } from 'react-router-dom'
import api from '../../api'
import Message from '../../components/message/Message'
import Breadcrumbs from '../../components/breadcrumbs/Breadcrumbs'
import Loader from '../../components/loader/Loader'

function SeatView() {
  const { aircraftId, num } = useParams()
  const [loaded, setLoaded] = useState(false)
  const [data, setData] = useState(null)
  const [error, setError] = useState(false)
  useEffect(() => {
    if (loaded) return
    api.seats
      .view(aircraftId, num)
      .then((response) => {
        if (response.ok) return setData(response.data)
        throw response
      })
      .catch((e) => {
        setLoaded(true)
        setError(
          <Message
            title={e.code || e.statusCode}
            message={e.error || e.statusText || e.message}
          />
        )
      })
      .finally(() => {
        setLoaded(true)
      })
  }, [])
  if (error) return error
  return (
    <>
      {!loaded && <Loader />}
      {loaded && (
        <React.Fragment>
          <Breadcrumbs
            links={[
              { title: 'Места', link: '/seats' },
              {
                title: `${data.seat_no} - ${data.aircraft_info.model} (${data.aircraft_info.aircraft_code}) - ${data.class_info.name}`,
              },
            ]}
          />
          <h1>{data.seat_no}</h1>
          <table className="table table-sm table-bordered">
            <tbody>
              <tr>
                <th>Самолет</th>
                <td>
                  {data.aircraft_info.model} ({data.aircraft_info.aircraft_code}
                  )
                </td>
              </tr>
              <tr>
                <th>Класс обслуживания</th>
                <td>{data.class_info.name}</td>
              </tr>
              <tr>
                <th>Номер места</th>
                <td>{data.seat_no}</td>
              </tr>
            </tbody>
          </table>
          <div
            className="btn-group"
            role="group"
            aria-label="Управление местами"
          >
            <Link
              to={`/seats/${data.aircraft_code}/${data.seat_no}/edit`}
              className="btn btn-warning"
            >
              Изменить
            </Link>
            <Link
              to={`/seats/${data.aircraft_code}/${data.seat_no}/delete`}
              className="btn btn-danger"
            >
              Удалить
            </Link>
          </div>
        </React.Fragment>
      )}
    </>
  )
}

export default SeatView
