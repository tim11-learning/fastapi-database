import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import api from '../../api'
import Breadcrumbs from '../../components/breadcrumbs/Breadcrumbs'
import Loader from '../../components/loader/Loader'

function SeatIndex() {
  const [data, setData] = useState([])
  const [loaded, setLoaded] = useState(false)
  useEffect(() => {
    api.seats.list().then((response) => {
      setLoaded(true)
      if (response.ok) return setData(response.data)
    })
  }, [])
  return (
    <>
      <Breadcrumbs links={[{ title: 'Места' }]} />
      <h1>Список мест</h1>
      <p>
        <Link to="/seats/add" className="btn btn-success">
          Добавить место
        </Link>
      </p>
      {!loaded && <Loader />}
      {loaded &&
        (() => {
          if (!data.length)
            return <div className="alert alert-warning">Список мест пуст</div>
          return (
            <table className="table table-bordered table-sm">
              <thead className="thead-dark">
                <tr>
                  <th scope="col">Номер места</th>
                  <th scope="col">Самолет</th>
                  <th scope="col">Класс обслуживания</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {(data || []).map((item) => {
                  return (
                    <React.Fragment
                      key={'seat-' + item.aircraft_code + '-' + item.seat_no}
                    >
                      <tr>
                        <th scope="row">
                          <Link
                            to={`/seats/${item.aircraft_code}/${item.seat_no}`}
                          >
                            {item.seat_no}
                          </Link>
                        </th>
                        <td>
                          {item.aircraft_info.model} (
                          {item.aircraft_info.aircraft_code})
                        </td>
                        <th scope="row">{item.class_info.name}</th>
                        <td
                          className="text-right"
                          style={{
                            width: '1%',
                            paddingLeft: '15px',
                            paddingRight: '15px',
                            whiteSpace: 'nowrap',
                          }}
                        >
                          <Link
                            to={`/seats/${item.aircraft_code}/${item.seat_no}/edit`}
                            title="Изменить"
                          >
                            <i className="fa-solid fa-pen-to-square" />
                          </Link>
                          &nbsp;
                          <Link
                            to={`/seats/${item.aircraft_code}/${item.seat_no}/delete`}
                            title="Удалить"
                            className="text-danger"
                          >
                            <i className="fa-regular fa-circle-xmark" />
                          </Link>
                        </td>
                      </tr>
                    </React.Fragment>
                  )
                })}
              </tbody>
            </table>
          )
        })()}
    </>
  )
}

export default SeatIndex
