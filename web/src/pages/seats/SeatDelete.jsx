import React, { useState, useEffect } from 'react'
import { useParams, Link, useNavigate } from 'react-router-dom'
import api from '../../api'
import Message from '../../components/message/Message'
import Breadcrumbs from '../../components/breadcrumbs/Breadcrumbs'
import Loader from '../../components/loader/Loader'

function SeatDelete() {
  const navigate = useNavigate()
  const { aircraftId, num } = useParams()
  const [loaded, setLoaded] = useState(false)
  const [data, setData] = useState()
  const [error, setError] = useState(false)
  useEffect(() => {
    if (loaded) return
    api.seats
      .view(aircraftId, num)
      .then((response) => {
        if (response.ok) return setData(response.data)
        throw response
      })
      .catch((e) => {
        setError(
          <Message
            title={e.code || e.statusCode}
            message={e.error || e.statusText || e.message}
          />
        )
      })
      .finally(() => {
        setLoaded(true)
      })
  }, [])
  if (error) return error
  const handleConfirm = (e) => {
    e.preventDefault()
    api.seats
      .delete(aircraftId, num)
      .then((response) => {
        if (!response.ok) throw response
        navigate(`/seats`)
      })
      .catch((response) => {
        alert(response.error)
      })
  }
  return (
    <div>
      {!data && <Loader />}
      {data && (
        <Breadcrumbs
          links={[
            { title: 'Места', link: '/seats' },
            {
              title: `${data.seat_no} - ${data.aircraft_info.model} (${data.aircraft_info.aircraft_code}) - ${data.class_info.name}`,
              link: `/seats/${data.aircraft_code}/${data.seat_no}`,
            },
            { title: 'Удаление места' },
          ]}
        />
      )}
      <h1>Удаление места</h1>
      {!loaded && <Loader />}
      {loaded && (
        <React.Fragment>
          <div className="alert alert-warning">
            Вы действительно хотите место{' '}
            <b>
              {data.seat_no} - {data.aircraft_info.model} (
              {data.aircraft_info.aircraft_code}) - {data.class_info.name}
            </b>
            ?
          </div>
          <Link to={'/seats'} className="btn btn-secondary">
            Нет
          </Link>
          &nbsp;
          <button className="btn btn-danger" onClick={handleConfirm}>
            Да
          </button>
        </React.Fragment>
      )}
    </div>
  )
}

export default SeatDelete
