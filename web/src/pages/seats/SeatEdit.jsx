import React, { useState, useEffect } from 'react'
import { useParams, useNavigate, Link } from 'react-router-dom'
import api from '../../api'
import Message from '../../components/message/Message'
import Breadcrumbs from '../../components/breadcrumbs/Breadcrumbs'
import Loader from '../../components/loader/Loader'

function SeatEdit() {
  const navigate = useNavigate()
  const { aircraftId, num: seatId } = useParams()
  const [loaded, setLoaded] = useState(false)
  const [aircrafts, setAircrafts] = useState([])
  const [classes, setClasses] = useState([])
  const [code, setCode] = useState()
  const [fare, setFare] = useState()
  const [data, setData] = useState()
  const [num, setNum] = useState('')
  const [error, setError] = useState(false)
  const [except, setExcept] = useState(false)
  const [disabled, setDisabled] = useState(false)
  useEffect(() => {
    if (loaded) return
    const promises = [api.aircrafts.list(), api.classes.list()]
    if (aircraftId && seatId) {
      promises.push(api.seats.view(aircraftId, seatId))
    }
    Promise.all(promises)
      .then((values) => {
        if (!values[0].ok) throw values[0]
        if (!values[1].ok) throw values[1]
        setAircrafts(
          (values[0].data || []).map((item) => {
            return { aircraft_code: item.aircraft_code, model: item.model }
          })
        )
        setClasses(
          (values[1].data || []).map((item) => {
            return { id: item.id, name: item.name }
          })
        )
        if (!aircraftId || !seatId) return
        if (!values[2].ok) throw values[2]
        const d = values[2].data
        setCode(d.aircraft_code)
        setFare(d.class_info.id)
        setNum(d.seat_no)
        setData(d)
      })
      .catch((e) => {
        setExcept(
          <Message
            title={e.code || e.statusCode}
            message={e.error || e.statusText || e.message}
          />
        )
      })
      .finally(() => {
        setLoaded(true)
      })
  })

  const links = [{ title: 'Места', link: '/seats' }]
  if (aircraftId && seatId && data) {
    links.push({
      title: `${data.seat_no} - ${data.aircraft_info.model} (${data.aircraft_info.aircraft_code}) - ${data.class_info.name}`,
      link: `/seats/${data.aircraft_code}/${data.seat_no}`,
    })
    links.push({ title: 'Изменение места' })
  } else if (!aircraftId || !seatId) {
    links.push({ title: 'Добавление места' })
  }
  if (except) return except

  const validate = () => {
    return !!code && !!fare && (num || '').toString().trim()
  }
  const handleSubmit = (e) => {
    e.preventDefault()
    if (!validate()) return
    setDisabled(true)
    const sendData = {
      aircraft_code: code,
      seat_no: num.trim().toUpperCase(),
      fare_conditions: fare,
    }
    const promise =
      aircraftId && seatId
        ? api.seats.update(aircraftId, seatId, sendData)
        : api.seats.create(sendData)
    promise
      .then((response) => {
        if (!response.ok) throw response
        navigate(
          `/seats/${response.data.aircraft_code}/${response.data.seat_no}`
        )
      })
      .catch((response) => {
        setError(response.error)
      })
      .finally(() => {
        setDisabled(false)
      })
  }
  return (
    <>
      <Breadcrumbs links={links} />
      <h1>{!aircraftId || !seatId ? 'Добавление места' : 'Изменение места'}</h1>
      {!loaded && <Loader />}
      {loaded &&
        (() => {
          if (!aircrafts.length) {
            return (
              <div className="alert alert-warning">
                <p>
                  Перед добавлением места Вам необходимо добавить хотя бы один
                  самолет
                </p>
                <Link to={'/aircrafts/add'} className="btn btn-success">
                  Добавить самолет
                </Link>
              </div>
            )
          }
          if (!classes.length) {
            return (
              <div className="alert alert-warning">
                <p>
                  Перед добавлением места Вам необходимо добавить хотя бы один
                  класс обслуживания
                </p>
                <Link to={'/classes/add'} className="btn btn-success">
                  Добавить класс обслуживания
                </Link>
              </div>
            )
          }
          return (
            <form onSubmit={handleSubmit}>
              {error && <div className="alert alert-danger">{error}</div>}
              <div className="form-group">
                <label htmlFor="seat-aircraft">Самолет</label>
                <select
                  className="form-control"
                  id="seat-aircraft"
                  name="code"
                  value={code}
                  onChange={(e) => setCode(e.target.value)}
                >
                  <option value="">Выберите самолет</option>
                  {aircrafts.map((aircraft) => {
                    return (
                      <React.Fragment
                        key={'seat-aircraft-' + aircraft.aircraft_code}
                      >
                        <option value={aircraft.aircraft_code}>
                          {aircraft.model} ({aircraft.aircraft_code})
                        </option>
                      </React.Fragment>
                    )
                  })}
                </select>
              </div>
              <div className="form-group">
                <label htmlFor="seat-class">Класс обслуживания</label>
                <select
                  className="form-control"
                  id="seat-class"
                  name="fare"
                  value={fare}
                  onChange={(e) => setFare(e.target.value)}
                >
                  <option value="">Выберите класс обслуживания</option>
                  {classes.map((fare) => {
                    return (
                      <React.Fragment key={'seat-class-' + fare.id}>
                        <option value={fare.id}>{fare.name}</option>
                      </React.Fragment>
                    )
                  })}
                </select>
              </div>
              <div className="form-group">
                <label htmlFor="seat-num">Номер места</label>
                <input
                  className="form-control"
                  maxLength="4"
                  type="text"
                  name="name"
                  id="seat-num"
                  value={num}
                  placeholder="Введите номер места"
                  onChange={(e) =>
                    setNum(
                      e.target.value.replace(/[^a-zA-Z0-9,]/g, '').toUpperCase()
                    )
                  }
                />
              </div>

              <button
                type="submit"
                className="btn btn-primary"
                disabled={disabled || !validate()}
              >
                {aircraftId && seatId ? 'Сохранить' : 'Добавить'}
              </button>
            </form>
          )
        })()}
    </>
  )
}

export default SeatEdit
