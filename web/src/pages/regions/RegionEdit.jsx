import React, { useState, useEffect } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import api from '../../api'
import Message from '../../components/message/Message'
import Breadcrumbs from '../../components/breadcrumbs/Breadcrumbs'
import Loader from '../../components/loader/Loader'

function RegionEdit() {
  const navigate = useNavigate()
  const { regionId: id } = useParams()
  const [loaded, setLoaded] = useState(false)
  const [data, setData] = useState()
  const [name, setName] = useState(null)
  const [error, setError] = useState(false)
  const [except, setExcept] = useState(false)
  const [disabled, setDisabled] = useState(false)
  useEffect(() => {
    if (!id) return setLoaded(true)
    if (loaded) return
    api.regions
      .view(id)
      .then((response) => {
        if (response.ok) {
          setData(response.data)
          return setName(response.data.name)
        }
        throw response
      })
      .catch((e) => {
        setExcept(
          <Message
            title={e.code || e.statusCode}
            message={e.error || e.statusText || e.message}
          />
        )
      })
      .finally(() => {
        setLoaded(true)
      })
  }, [])
  if (except) return except

  const validate = () => {
    return (name || '').toString().trim()
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    if (!validate()) return
    setDisabled(true)
    const sendData = { name: name.trim() }
    const promise = id
      ? api.regions.update(id, sendData)
      : api.regions.create(sendData)
    promise
      .then((response) => {
        if (!response.ok) throw response
        navigate(`/regions/${response.data.id}`)
      })
      .catch((response) => {
        setError(response.error)
      })
      .finally(() => {
        setDisabled(false)
      })
  }
  const links = [{ title: 'Регионы', link: '/regions' }]
  if (data && id) {
    links.push({ title: data.name, link: '/regions/' + id })
    links.push({ title: 'Изменение региона' })
  } else if (!id) {
    links.push({ title: 'Добавление региона' })
  }
  return (
    <>
      <Breadcrumbs links={links} />
      <h1>{!id ? 'Добавление региона' : 'Изменение региона'}</h1>
      {!loaded && <Loader />}
      {loaded && (
        <form onSubmit={handleSubmit}>
          {error && <div className="alert alert-danger">{error}</div>}
          <div className="form-group">
            <label htmlFor="region-name">Название региона</label>
            <input
              className="form-control"
              type="text"
              name="name"
              id="region-name"
              value={name}
              placeholder="Введите название региона"
              onChange={(e) => setName(e.target.value)}
            />
          </div>
          <button
            type="submit"
            className="btn btn-primary"
            disabled={disabled || !validate()}
          >
            {id ? 'Сохранить' : 'Добавить'}
          </button>
        </form>
      )}
    </>
  )
}

export default RegionEdit
