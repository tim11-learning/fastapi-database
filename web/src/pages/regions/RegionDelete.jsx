import React, { useState, useEffect } from 'react'
import { useParams, Link, useNavigate } from 'react-router-dom'
import api from '../../api'
import Message from '../../components/message/Message'
import Breadcrumbs from '../../components/breadcrumbs/Breadcrumbs'
import Loader from '../../components/loader/Loader'

function RegionDelete() {
  const navigate = useNavigate()
  const { regionId: id } = useParams()
  const [data, setData] = useState(null)
  const [error, setError] = useState(false)
  useEffect(() => {
    api.regions
      .view(id)
      .then((response) => {
        if (response.ok) return setData(response.data)
        throw response
      })
      .catch((e) => {
        setError(
          <Message
            title={e.code || e.statusCode}
            message={e.error || e.statusText || e.message}
          />
        )
      })
  }, [])
  if (error) return error
  const handleConfirm = (e) => {
    e.preventDefault()
    api.regions
      .delete(id)
      .then((response) => {
        if (!response.ok) throw response
        navigate(`/regions`)
      })
      .catch((response) => {
        alert(response.error)
      })
  }
  return (
    <div>
      {!data && <Loader />}
      {data && (
        <Breadcrumbs
          links={[
            { title: 'Регионы', link: '/regions' },
            { title: data.name, link: '/regions/' + data.id },
            { title: 'Удаление региона' },
          ]}
        />
      )}
      <h1>Удаление региона</h1>
      {!data && (
        <React.Fragment>
          <div>Loading</div>
        </React.Fragment>
      )}
      {data && (
        <React.Fragment>
          <div className="alert alert-warning">
            Вы действительно хотите удалить регион <b>{data.name}</b>?
          </div>
          <Link to={'/regions'} className="btn btn-secondary">
            Нет
          </Link>
          &nbsp;
          <button className="btn btn-danger" onClick={handleConfirm}>
            Да
          </button>
        </React.Fragment>
      )}
    </div>
  )
}

export default RegionDelete
