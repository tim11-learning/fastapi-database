import React from 'react'
import Layout from './components/layout/Layout'
import Error from './pages/error/Error'

import RegionIndex from './pages/regions/RegionIndex'
import RegionView from './pages/regions/RegionView'
import RegionDelete from './pages/regions/RegionDelete'
import RegionEdit from './pages/regions/RegionEdit'

import CityIndex from './pages/cities/CityIndex'
import CityEdit from './pages/cities/CityEdit'
import CityView from './pages/cities/CityView'
import CityDelete from './pages/cities/CityDelete'
import ClassIndex from './pages/classes/ClassIndex'
import ClassEdit from './pages/classes/ClassEdit'
import ClassView from './pages/classes/ClassView'
import ClassDelete from './pages/classes/ClassDelete'
import AircraftIndex from './pages/aircrafts/AircraftIndex'
import AircraftEdit from './pages/aircrafts/AircraftEdit'
import AircraftView from './pages/aircrafts/AircraftView'
import AircraftDelete from './pages/aircrafts/AircraftDelete'
import SeatIndex from './pages/seats/SeatIndex'
import SeatEdit from './pages/seats/SeatEdit'
import SeatView from './pages/seats/SeatView'
import SeatDelete from './pages/seats/SeatDelete'

const routes = [
  {
    path: '/',
    element: <Layout />,
    errorElement: <Error />,
    children: [
      {
        path: 'regions',
        element: <RegionIndex />,
      },
      {
        path: 'regions/:regionId',
        element: <RegionView />,
      },
      {
        path: 'regions/:regionId/edit',
        element: <RegionEdit />,
      },
      {
        path: 'regions/add',
        element: <RegionEdit />,
      },
      {
        path: 'regions/:regionId/delete',
        element: <RegionDelete />,
      },
      {
        path: 'cities',
        element: <CityIndex />,
      },
      {
        path: 'cities/:cityId',
        element: <CityView />,
      },
      {
        path: 'cities/:cityId/edit',
        element: <CityEdit />,
      },
      {
        path: 'cities/add',
        element: <CityEdit />,
      },
      {
        path: 'cities/:cityId/delete',
        element: <CityDelete />,
      },
      {
        path: 'classes',
        element: <ClassIndex />,
      },
      {
        path: 'classes/:classId/edit',
        element: <ClassEdit />,
      },
      {
        path: 'classes/add',
        element: <ClassEdit />,
      },
      {
        path: 'classes/:classId',
        element: <ClassView />,
      },
      {
        path: 'classes/:classId/delete',
        element: <ClassDelete />,
      },
      {
        path: 'aircrafts',
        element: <AircraftIndex />,
      },
      {
        path: 'aircrafts/:aircraftId/edit',
        element: <AircraftEdit />,
      },
      {
        path: 'aircrafts/add',
        element: <AircraftEdit />,
      },
      {
        path: 'aircrafts/:aircraftId',
        element: <AircraftView />,
      },
      {
        path: 'aircrafts/:aircraftId/delete',
        element: <AircraftDelete />,
      },
      {
        path: 'seats',
        element: <SeatIndex />,
      },
      {
        path: 'seats/:aircraftId/:num',
        element: <SeatView />,
      },
      {
        path: 'seats/:aircraftId/:num/edit',
        element: <SeatEdit />,
      },
      {
        path: 'seats/add',
        element: <SeatEdit />,
      },
      {
        path: 'seats/:aircraftId/:num/delete',
        element: <SeatDelete />,
      },
    ],
  },
]

export default routes
