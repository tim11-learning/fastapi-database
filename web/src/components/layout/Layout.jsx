import React from 'react'
import { Outlet, Link } from 'react-router-dom'
import './Layout.css'

function Layout() {
  return (
    <main className="layout">
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <Link to="/" className="navbar-brand">
          Airport Admin panel
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbar-top"
          aria-controls="navbar-top"
          aria-expanded="false"
          aria-label="Open menu"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbar-top">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item dropdown">
              <span
                className="nav-link dropdown-toggle"
                id="dropdown-geo"
                role="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Гео
              </span>
              <div className="dropdown-menu" aria-labelledby="dropdown-geo">
                <Link to="/regions" className="dropdown-item">
                  Регионы
                </Link>
                <Link to="/cities" className="dropdown-item">
                  Города
                </Link>
              </div>
            </li>
            <li className="nav-item dropdown">
              <span
                className="nav-link dropdown-toggle"
                id="dropdown-air"
                role="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Самолеты
              </span>
              <div className="dropdown-menu" aria-labelledby="dropdown-geo">
                <Link to="/classes" className="dropdown-item">
                  Классы обслуживания
                </Link>
                <Link to="/aircrafts" className="dropdown-item">
                  Список самолетов
                </Link>
                <Link to="/seats" className="dropdown-item">
                  Места
                </Link>
              </div>
            </li>
          </ul>
        </div>
      </nav>
      <div className="container-fluid">
        <Outlet />
      </div>
    </main>
  )
}

export default Layout
