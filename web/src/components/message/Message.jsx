import React from 'react'
import './Message.css'

function Message(props = {}) {
  const { title, message, style = 'default' } = props || {}
  return (
    <div className={'message message--' + style}>
      <div className="message__header">{title}</div>
      <div className="message__body">{message}</div>
    </div>
  )
}

export default Message
