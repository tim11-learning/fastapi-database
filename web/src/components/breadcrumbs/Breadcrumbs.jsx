import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

function Breadcrumbs(props = { links: [] }) {
  const links = props?.links || []
  if (!links.length) return <React.Fragment></React.Fragment>
  // <li className="breadcrumb-item active" aria-current="page">
  return (
    <nav aria-label="breadcrumb">
      <ol className="breadcrumb">
        {links.map((item, index) => {
          return (
            <React.Fragment key={'breadcrumb-' + index}>
              <li className="breadcrumb-item">
                {item.link && <Link to={item.link}>{item.title}</Link>}
                {!item.link && <>{item.title}</>}
              </li>
            </React.Fragment>
          )
        })}
      </ol>
    </nav>
  )
}

Breadcrumbs.propTypes = {
  links: PropTypes.array,
}
export default Breadcrumbs
