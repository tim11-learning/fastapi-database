function request(url, method = 'GET', data = {}, headers = {}) {
  return fetch('/api' + url, {
    method,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'X-Token': '123456789',
      ...(headers || {}),
    },
    ...(Object.keys(data).length ? { body: JSON.stringify(data) } : {}),
  }).then((response) => {
    const type = response.headers.get('content-type')
    const isJson = type && type.includes('application/json')
    const answer = {
      ok: response.ok,
      code: response.status || response.statusCode,
    }
    if (+answer.code === 204) return answer
    return (isJson ? response.json() : response.text).then((data) => {
      if (answer.ok) Object.assign(answer, { data })
      return Object.assign(answer, {
        error: data?.detail || response.statusText,
      })
    })
  })
}

const api = {
  regions: {
    list: () => request('/regions'),
    view: (id) => request(`/regions/${id}`),
    create: (data) => request(`/regions`, 'POST', data),
    update: (id, data) => request(`/regions/${id}`, 'PUT', data),
    delete: (id) => request(`/regions/${id}`, 'DELETE'),
  },
  cities: {
    list: () => request('/cities'),
    view: (id) => request(`/cities/${id}`),
    create: (data) => request(`/cities`, 'POST', data),
    update: (id, data) => request(`/cities/${id}`, 'PUT', data),
    delete: (id) => request(`/cities/${id}`, 'DELETE'),
  },
  classes: {
    list: () => request('/classes'),
    view: (id) => request(`/classes/${id}`),
    create: (data) => request(`/classes`, 'POST', data),
    update: (id, data) => request(`/classes/${id}`, 'PUT', data),
    delete: (id) => request(`/classes/${id}`, 'DELETE'),
  },
  aircrafts: {
    list: () => request('/aircrafts'),
    view: (code) => request(`/aircrafts/${code}`),
    create: (data) => request(`/aircrafts`, 'POST', data),
    update: (code, data) => request(`/aircrafts/${code}`, 'PUT', data),
    delete: (code) => request(`/aircrafts/${code}`, 'DELETE'),
  },
  seats: {
    list: () => request('/seats'),
    view: (code, num) => request(`/seats/${code}-${num}`),
    create: (data) => request(`/seats`, 'POST', data),
    update: (code, num, data) => request(`/seats/${code}-${num}`, 'PUT', data),
    delete: (code, num) => request(`/seats/${code}-${num}`, 'DELETE'),
  },
}

export default api
